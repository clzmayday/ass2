import itertools


def morseCharacterDecorde(inputChar):
        out = {
                '.-':'A',
                '-...':'B',
                '-.-.':'C',
                '-..':'D',
                '.':'E',
                '..-.':'F',
                '--.':'G',
                '....':'H',
                '..':'I',
                '.---':'J',
                '-.-':'K',
                '.-..':'L',
                '--':'M',
                '-.':'N',
                '---':'O',
                '.--.':'P',
                '--.-':'Q',
                '.-.':'R',
                '...':'S',
                '-':'T',
                '..-':'U',
                '...-':'V',
                '.--':'W',
                '-..-':'X',
                '-.--':'Y',
                '--..':'Z',
                '.----':'1',
                '..---':'2',
                '...--':'3',
                '....-':'4',
                '.....':'5',
                '-....':'6',
                '--...':'7',
                '---..':'8',
                '----.':'9',
                '-----':'0',
        }
        
        return out.get(inputChar, "Error Input")
        
        
def morseDecode(inputStringList):
        """
        This method should take a list of strings as input. Each string is equivalent to one letter
        (i.e. one morse code string). The entire list of strings represents a word.

        This method should convert the strings from morse code into english, and return the word as a string.

        """
        # Please complete this method to perform the above described function
        decodeString = list(map(lambda x: morseCharacterDecorde(x), inputStringList))
                
        return ''.join(decodeString)

def count_x(inputStringList):
        count = 0
        for char in inputStringList:
                        for single in char:
                                if single == 'x':
                                        count += 1
        return count


def pattern_match(pattern, word):
        count = 0
        word_out = []
        for each in range(len(word)):
                char_out = ''
                for cha in range(len(word[each])):
                        if word[each][cha] == 'x':
                                char_out += pattern[count]
                                count += 1
                        else:
                                char_out += word[each][cha]
                word_out.append(char_out)
        return word_out
                                

def morsePartialDecode(inputStringList):
        """
        This method should take a list of strings as input. Each string is equivalent to one letter
        (i.e. one morse code string). The entire list of strings represents a word.

        However, the first character of every morse code string is unknown (represented by an 'x' (lowercase))
        For example, if the word was originally TEST, then the morse code list string would normally be:
        ['-','.','...','-']

        However, with the first characters missing, I would receive:
        ['x','x','x..','x']

        With the x unknown, this word could be TEST, but it could also be EESE or ETSE or ETST or EEDT or other permutations.

        We define a valid words as one that exists within the dictionary file provided on the website, dictionary.txt
        When using this file, please always use the location './dictionary.txt' and place it in the same directory as
        the python script.

        This function should find and return a list of strings of all possible VALID words.
        """

        dictionaryFileLoc = './dictionary.txt'
        dictionary = list(map(lambda x: x.upper().replace('\n', ''), open(dictionaryFileLoc, 'r').readlines()))
        # Please complete this method to perform the above described function
        out = []
        combination = list(itertools.product('-.', repeat=count_x(inputStringList)))
        
        for pattern in combination:
                if morseDecode(pattern_match(pattern, inputStringList)) in dictionary:
                        out.append(morseDecode(pattern_match(pattern, inputStringList)))
        return out
class tree:
    def __init__(self, point):
        self.point = point
        self.up = None
        self.down = None
        self.left = None
        self.right = None
        
    def getPoint(self):
        return self.point
    def getLeft(self):
        return self.up
    def getRight(self):
        return self.right
    def getUp(self):
        return self.up
    def getDown(self):
        return self.down
    def setLeft(self, l):
        self.left = l
    def setRight(self, r):
        self.right = r
    def setUp(self, u):
        self.up = u
    def setDown(self, d):
        self.down = d
    def setPoint(self, point):
        self.point = point 
        
class Maze:
        def __init__(self):
                """
                Constructor - You may modify this, but please do not add any extra parameters
                """
                self.grid = []
                self.row_list = []
                self.col_list = []
                self.open_list = []
                self.max_row = 0
                self.max_col = 0

        def addCoordinate(self,x,y,blockType):
                """
                Add information about a coordinate on the maze grid
                x is the x coordinate
                y is the y coordinate
                blockType should be 0 (for an open space) of 1 (for a wall)
                """

                # Please complete this method to perform the above described function
                self.grid.append((x,y,blockType))
                self.manage()
                

        def printMaze(self):
                """
                Print out an ascii representation of the maze.
                A * indicates a wall and a empty space indicates an open space in the maze
                """

                # Please complete this method to perform the above described function
                for row in range(self.max_row+2):
                    for col in range(self.max_col+2):
                        if (col, row, 0) in self.grid:
                            print(' ', end='')
                        else:
                            print('*', end='')
                    print()
                    
        def build_tree(self, coor, save_list):
            """
            0 - start
            1 - up
            2 - down
            3 - left
            4 - right
            """
            q_tree = tree(coor)
            if coor not in self.open_list:
                return None
            else:
                if coor not in save_list:
                    save_list.append(coor)
                    q_tree.up = self.build_tree((coor[0], coor[1]-1), save_list)
                    q_tree.down = self.build_tree((coor[0], coor[1]+1), save_list)
                    q_tree.left = self.build_tree((coor[0]-1, coor[1]), save_list)
                    q_tree.right = self.build_tree((coor[0]+1, coor[1]), save_list)
                    return q_tree
                else:
                  return None
                
        def bfs(self, q_tree):
          if q_tree != None:
            if q_tree.getPoint() == self.target:
              self.visit.append(q_tree.getPoint())
              self.path.append(q_tree.getPoint())
              return 1
            else:
              if q_tree.getPoint() not in self.visit:
                self.visit.append(q_tree.getPoint())
                if self.bfs(q_tree.up) == 1 or self.bfs(q_tree.down) == 1 or self.bfs(q_tree.left) == 1 or self.bfs(q_tree.right) == 1:
                  self.path.insert(0, q_tree.getPoint())
                  return 1
                else:
                  return 0
              else:
                return 0
          else:
            return 0
            
          
            
        def findRoute(self,x1,y1,x2,y2):
                """
                This method should find a route, traversing open spaces, from the coordinates (x1,y1) to (x2,y2)
                It should return the list of traversed coordinates followed along this route as a list of tuples (x,y),
                in the order in which the coordinates must be followed
                If no route is found, return an empty list
                """

                self.tree = self.build_tree((x1, y1), [])
                self.visit = []
                self.path = []
                self.target = (x2, y2)
                self.bfs(self.tree)
                
                return self.path
              
        def manage(self):
            self.row_list = []
            self.col_list = []
            self.open_list = []
            self.max_row = 0
            self.max_col = 0
            for point in self.grid:
                self.col_list.append(point[0])
                self.row_list.append(point[1])
                if point[2] == 0:
                    self.open_list.append((point[0], point[1]))
            if len(self.open_list) != 0:
              self.max_row = max(self.row_list)
              self.max_col = max(self.col_list)
def morseCodeTest():
        """
        This test program passes the morse code as a list of strings for the word
        HELLO to the decode method. It should receive a string "HELLO" in return.
        This is provided as a simple test example, but by no means covers all possibilities, and you should
        fulfill the methods as described in their comments.
        """

        hello = ['....','.','.-..','.-..','---']
        print(morseDecode(hello))

def partialMorseCodeTest():

        """
        This test program passes the partial morse code as a list of strings 
        to the morsePartialDecode method. This is provided as a simple test example, but by
        no means covers all possibilities, and you should fulfill the methods as described in their comments.
        """

        # This is a partial representation of the word TEST, amongst other possible combinations
        test = ['x','x','x..','x']
        print(morsePartialDecode(test))

        # This is a partial representation of the word DANCE, amongst other possible combinations
        dance = ['x..','x-','x.','x.-.','x']
        print(morsePartialDecode(dance))
        
def mazeTest():
        """
        This sets the open space coordinates for the example
        maze in the assignment.
        The remainder of coordinates within the max bounds of these specified coordinates
        are assumed to be walls
        """
        myMaze = Maze()
        myMaze.addCoordinate(1,0,0)
        myMaze.addCoordinate(1,1,0)
        myMaze.addCoordinate(7,1,0)
        myMaze.addCoordinate(1,2,0)
        myMaze.addCoordinate(2,2,0)
        myMaze.addCoordinate(3,2,0)
        myMaze.addCoordinate(4,2,0)
        myMaze.addCoordinate(6,2,0)
        myMaze.addCoordinate(7,2,0)
        myMaze.addCoordinate(4,3,0)
        myMaze.addCoordinate(7,3,0)
        myMaze.addCoordinate(4,4,0)
        myMaze.addCoordinate(7,4,0)
        myMaze.addCoordinate(3,5,0)
        myMaze.addCoordinate(4,5,0)
        myMaze.addCoordinate(7,5,0)
        myMaze.addCoordinate(1,6,0)
        myMaze.addCoordinate(2,6,0)
        myMaze.addCoordinate(3,6,0)
        myMaze.addCoordinate(3,7,1)
        myMaze.addCoordinate(4,6,0)
        myMaze.addCoordinate(5,6,0)
        myMaze.addCoordinate(6,6,0)
        myMaze.addCoordinate(7,6,0)
        myMaze.addCoordinate(5,7,0)
        myMaze.printMaze()
        print(myMaze.findRoute(1,0,1,1))
        
def main():
        morseCodeTest()
        partialMorseCodeTest()
        mazeTest()

if(__name__ == "__main__"):
        main()
